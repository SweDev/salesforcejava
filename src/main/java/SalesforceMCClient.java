


import com.exacttarget.fuelsdk.ETClient;
import com.exacttarget.fuelsdk.ETSdkException;
import com.exacttarget.fuelsdk.ETConfiguration;
import com.exacttarget.fuelsdk.ETEmail;
import com.exacttarget.fuelsdk.ETObject;
import com.exacttarget.fuelsdk.internal.APIObject;
import com.exacttarget.fuelsdk.internal.ContentArea;
import com.exacttarget.fuelsdk.internal.CreateOptions;
import com.exacttarget.fuelsdk.internal.CreateRequest;
import com.exacttarget.fuelsdk.internal.CreateResponse;
import com.exacttarget.fuelsdk.internal.Soap;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * SalesforceMCClient class.
 *
 * @author pparadis
 */
public class SalesforceMCClient {

    static final String USERNAME     = "Patrik_Cap";
    static final String PASSWORD     = "g!H7Nkm317fcq";
    static final String POSTURL     = "auth.exacttargetapis.com";
    static final String GRANTSERVICE = "/v1/requestToken";
    static final String CLIENTID     = "0b6wi7hg557rmpxeci6p5u1e";
    static final String CLIENTSECRET = "D5Psci4D5C3vQv8diMMHnkSk";
    
    public static void main(String[] args) throws ETSdkException, RemoteException {
        
        /////////// Connect & Authenticate /////////////
        /////////// Get the Access Token   /////////////
        ////////////////////////////////////////////////
//        Map<String, String> userCredentials = new HashMap<String, String>();
//        userCredentials.put("clientId", CLIENTID);
//        userCredentials.put("clientSecret", CLIENTSECRET); 
        System.out.println("__________Get Access Token__________");

        HttpClient httpclient = HttpClientBuilder.create().build();
 
        JSONObject holder = new JSONObject();
        holder.put("clientId", CLIENTID);
        holder.put("clientSecret", CLIENTSECRET);
        
        StringEntity se = null;
        try {
            se = new StringEntity(holder.toString());
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(SalesforceMCClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        // Assemble the Post request URL
        String PostPath = "https://" +
                          POSTURL +
                          GRANTSERVICE;
        
        // Login requests must be POSTs
        HttpPost httpPost = new HttpPost(PostPath);
        httpPost.setEntity(se);
        httpPost.addHeader("Accept","Application/json");
        httpPost.addHeader("Content-type","application/json");
        
        HttpResponse response = null;

        try {
            // Execute the login POST request
            response = httpclient.execute(httpPost);
        } catch (ClientProtocolException cpException) {
            cpException.printStackTrace();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        
        // verify response is HTTP OK
        final int statusCode = response.getStatusLine().getStatusCode();
        if (statusCode != HttpStatus.SC_OK) {
            System.out.println("Error authenticating to Force.com: "+statusCode);
            // Error is in EntityUtils.toString(response.getEntity())
            return;
        }

        
        String getResult = null;
        try {
            getResult = EntityUtils.toString(response.getEntity());
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        JSONObject jsonObject = null;
        String accessToken = null;
        int expiresIn = 0;
        try {
            jsonObject = (JSONObject) new JSONTokener(getResult).nextValue();
            accessToken = jsonObject.getString("accessToken");
            expiresIn = jsonObject.getInt("expiresIn");
        } catch (JSONException jsonException) {
            jsonException.printStackTrace();
        }
        System.out.println(response.getStatusLine());
        System.out.println("Successful login");
        System.out.println("  expires in: "+expiresIn);
        System.out.println("  access token/session ID: "+accessToken);
                
        // release connection
        httpPost.releaseConnection();
 
        /////////// Create content Area /////////////////
        /////////////////////////////////////////////////
        System.out.println("__________Create Content Area__________");
        String baseUri = ""; // TODO: Create the uri..
        String uri = baseUri + "";  // TODO: Add the resource URI
        
        // Construct the content object to be inserted
        JSONObject content = new JSONObject(); // TODO: Figure out how to create a Content Area json object.
        System.out.println("JSON object for content to be inserted:" + content.toString(1));
        
        // Configure the request
        HttpClient client = HttpClientBuilder.create().build();
        
    }
}
