import com.exacttarget.fuelsdk.ETClient;
import com.exacttarget.fuelsdk.ETConfiguration;
import com.exacttarget.fuelsdk.ETRestConnection;
import com.exacttarget.fuelsdk.ETSdkException;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pparadis
 */
public class SalesforceMCClientFuelSDK {
    String clientId = "0b6wi7hg557rmpxeci6p5u1e";
    String clientSecret = "D5Psci4D5C3vQv8diMMHnkSk";
    String username = null;
    String password = null;
    String authEndpoint = null;
    String endpoint = null;
    String autoHydrateObjects = null;

    public void run() {

        try {
            ETConfiguration conf = new ETConfiguration();
            conf.set("clientId", clientId);
            conf.set("clientSecret", clientSecret);
//        conf.set("username", username);
//        conf.set("password", password);
//        conf.set("authEndpoint", authEndpoint);
//        conf.set("endpoint", endpoint);
//        conf.set("autoHydrateObjects", autoHydrateObjects);

            ETClient etClient = new ETClient(conf);

            ETRestConnection connection = etClient.getRestConnection();

            ETRestConnection.Response responsecreate = connection.post("/asset/v1/content/assets", jsoncreate );
            System.out.println(responsecreate.getResponseCode());

            ETRestConnection.Response responsequery = connection.post("/asset/v1/content/assets/query", jsonquery );
            System.out.println(responsequery.getResponseCode());


        } catch (ETSdkException ex) {
            ex.printStackTrace();
        }


    }

    String jsonquery = "{\n" +
            "    \"page\":\n" +
            "    {\n" +
            "        \"page\":1,\n" +
            "        \"pageSize\":50\n" +
            "    },\n" +
            "\n" +
            "    \"query\":\n" +
            "    {\n" +
            "        \"leftOperand\":\n" +
            "        {\n" +
            "            \"property\":\"version\",\n" +
            "            \"simpleOperator\":\"equal\",\n" +
            "            \"value\":1\n" +
            "        },\n" +
            "        \"logicalOperator\":\"AND\",\n" +
            "        \"rightOperand\":\n" +
            "        {\n" +
            "            \"property\":\"name\",\n" +
            "            \"simpleOperator\":\"equal\",\n" +
            "            \"value\":\"Michael\"\n" +
            "        }\n" +
            "    },\n" +
            "\n" +
            "    \"sort\":\n" +
            "    [\n" +
            "        { \"property\":\"id\", \"direction\":\"ASC\" },\n" +
            "        { \"property\":\"version\", \"direction\":\"DESC\" }\n" +
            "    ],\n" +
            "\n" +
            "    \"fields\":\n" +
            "    [\n" +
            "        \"legacy\",\n" +
            "        \"content\",\n" +
            "        \"meta\"\n" +
            "    ]\n" +
            "}";


    String jsoncreate = "      {\n" +
            "         \"assetType\": {\n" +
            "            \"id\": 196,\n" +
            "            \"name\": \"textblock\"\n" +
            "         },\n" +
            "         \"name\": \"Michael\",\n" +
            "         \"owner\": {\n" +
            "            \"id\": 7515065,\n" +
            "            \"email\": \"patrik.paradis@capgeminisogeti.dk\",\n" +
            "            \"name\": \"Patrik Paradis (Capgemini)\",\n" +
            "            \"userId\": \"7515065\"\n" +
            "         },\n" +
            "         \"category\": {\n" +
            "            \"id\": 86005,\n" +
            "            \"parentId\": 0,\n" +
            "            \"name\": \"Content Builder\"\n" +
            "         },\n" +
            "         \"content\": \"<div data-marker=\\\"wrapper\\\" class=\\\"stylingblock-content-wrapper\\\" style=\\\"\\\">Hello from Michael!</div>\",\n" +
            "         \"data\": {\n" +
            "            \"email\": {\n" +
            "                  \"legacy\": {\n" +
            "                  \"legacyId\": \"50633\",\n" +
            "                  \"legacyType\": \"ContentArea\",\n" +
            "                  \"legacyCategoryId\": \"37933\"\t\t\t\n" +
            "               }\n" +
            "            }\n" +
            "         },\n" +
            "         \"modelVersion\": 2\n" +
            "      }";
}
